let fs = require('fs'),
    path = require('path'),
    aws = JSON.parse(fs.readFileSync('./../config/aws.json')),
    args = require('yargs').argv,
    gulp = require('gulp'),
    replace = require('gulp-replace-task');

let env;

if (!args.env || typeof args.env !== 'string') {
    env = 'dev';
} else {
    env = args.env;
}

let config = aws[env]['config'];
let bucket = aws[env]['bucket'];

let s3 = require('gulp-s3-upload')(config);

gulp.task('default', function() {

    gulp.src('./../dist/**')
        .pipe(s3({
            Bucket: bucket,
            ACL: 'public-read'
        }));

});
