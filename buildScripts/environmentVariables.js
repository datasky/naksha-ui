import path from 'path';
import chalk from 'chalk';

let fs = require('fs'),
    fsPath = require('fs-path'),
    args = require('yargs').argv;

let env;

if (!args.env || typeof args.env !== 'string') {
    env = 'dev';
} else {
    env = args.env;
}

console.log(chalk.green(`Creating variables for ${env}`));

let basePath = process.env.PWD;

let inputFileName = path.resolve(basePath, `environments/${env}.json`);
let settings = JSON.parse(fs.readFileSync(inputFileName, 'utf8'));

let outputFile = path.resolve(basePath, 'environments/env.module.js');
let outputFilePath = path.resolve(basePath, 'src/app/env/env.module.js');
let outputFileString = fs.readFileSync(outputFile, 'utf8');

Object.keys(settings).forEach(key => {

    if (outputFileString.includes(key)) {
        outputFileString = outputFileString.replace(`@@${key}`, settings[key]);
    }

});

fsPath.writeFile(outputFilePath, outputFileString, function(err) {
    if (err) {
        throw err;
    }
});
