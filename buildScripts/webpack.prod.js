import webpack from 'webpack';
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import WebpackMd5Hash from 'webpack-md5-hash';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

const nodeModulesDir = path.resolve(__dirname, 'node_modules');

export default {
    debug: false,
    noInfo: false,
    entry: {
        vendor: path.resolve(__dirname, './../src/vendor'),
        main: path.resolve(__dirname, './../src/main')
    },
    target: 'web',
    output: {
        path: path.resolve(__dirname, './../dist/assets/'),
        publicPath: '/assets/',
        filename: '[name].[chunkhash].js'
    },
    plugins: [
        // Hash the files using MD5 so that their names change when the content changes.
        new WebpackMd5Hash(),

        // Generate an external css file with a hash in the filename
        new ExtractTextPlugin('[name].[chunkhash].css'),

        // Optimize the order that items are bundled. This assures the hash is deterministic.
        new webpack.optimize.OccurenceOrderPlugin(),

        // Generate HTML file that contains references to generated bundles. See here for how this works: https://github.com/ampedandwired/html-webpack-plugin#basic-usage
        new HtmlWebpackPlugin({
            template: '!!pug!src/index.pug',
            favicon: 'src/favicon.ico',
            filename: "../index.html",
            minify: {
                removeComments: true,
                collapseWhitespace: false,
                removeRedundantAttributes: true,
                useShortDoctype: true,
                removeEmptyAttributes: true,
                removeStyleLinkTypeAttributes: true,
                keepClosingSlash: true,
                minifyJS: true,
                minifyCSS: true,
                minifyURLs: true
            },
            inject: true,
            chunksSortMode: function(chunk1, chunk2) {
                var orders = ['vendor', 'main'];
                var order1 = orders.indexOf(chunk1.names[0]);
                var order2 = orders.indexOf(chunk2.names[0]);
                if (order1 > order2) {
                    return 1;
                } else if (order1 < order2) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }),

        // Eliminate duplicate packages when generating bundle
        new webpack.optimize.DedupePlugin(),

        // Minify JS
        new webpack.optimize.UglifyJsPlugin()
    ],
    module: {
        loaders: [{
            test: /\.js?$/,
            exclude: [nodeModulesDir],
            loader: 'ng-annotate!babel?compact=true'
        }, {
            test: /\.css$/,
            exclude: [nodeModulesDir],
            loader: ExtractTextPlugin.extract("style-loader", "css"),
        }, {
            test: /\.scss$/,
            loader: ExtractTextPlugin.extract("style-loader", "css!sass"),
            exclude: [nodeModulesDir]
        }, {
            test: /\.html$/,
            loader: 'html',
            exclude: [nodeModulesDir,  /index.html$/]
        }, {
            test: /\.(png|jpg|jpeg|gif)$/,
            loader: 'url?limit=10000?name=images/[name].[hash].[ext]'
        }, {
            test: /\.((woff2?|svg)(\?v=[0-9]\.[0-9]\.[0-9]))|(woff2?|svg)$/,
            loader: "url?limit=10000&name=fonts/[name].[hash].[ext]"
        }, {
            test: /\.((ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9]))|(ttf|eot)$/,
            loader: "file?name=fonts/[name].[hash].[ext]"
        }]
    }
};
