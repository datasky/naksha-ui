import path from 'path';
import webpack from 'webpack';
import chalk from 'chalk';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

let argv = require('yargs').argv;
let DEV_SERVER_PORT = 3000;

if(typeof argv.portNumber === 'number') {
    DEV_SERVER_PORT = argv.portNumber;
}

console.log(chalk.blue(`Starting server of ${DEV_SERVER_PORT}.`));

const nodeModulesDir = path.resolve(__dirname, 'node_modules');

export default {
    debug: true,
    devtool: 'eval',
    devServer: {
        contentBase: './src',
        compress: true,
        port: DEV_SERVER_PORT,
        stats: 'minimal',
        historyApiFallback: {
            index: '/'
        }
    },
    noInfo: false,
    entry: {
        vendor: path.resolve(__dirname, './../src/vendor'),
        main: [path.resolve(__dirname, './../src/main')]
    },
    target: 'web',
    output: {
        pathinfo: true,
        path: path.resolve(__dirname, './../src'),
        publicPath: '/',
        filename: '[name].js'
    },
    plugins: [
        // Create HTML file that includes reference to bundled JS.
        new HtmlWebpackPlugin({
            template: '!!pug!src/index.pug',
            favicon: 'src/favicon.ico',
            inject: true,
            devServer: 'http://localhost:' + DEV_SERVER_PORT,
            baseHref: '/',
        }),
        new ExtractTextPlugin("[name].css", { allChunks: false }),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ],
    module: {
        loaders: [{
            test: /\.js?$/,
            exclude: [nodeModulesDir],
            loader: 'ng-annotate!babel?compact=false'
        }, {
            test: /\.css$/,
            exclude: [nodeModulesDir],
            loader: ExtractTextPlugin.extract("style-loader", "css"),
        }, {
            test: /\.scss$/,
            loader: ExtractTextPlugin.extract("style-loader", "css!sass"),
            exclude: [nodeModulesDir]
        }, {
            test: /\.html$/,
            loader: 'html',
            exclude: [nodeModulesDir]
        }, {
            // Load images
            test: /\.(png|jpg|jpeg|gif)$/,
            loader: 'url?limit=5000&name=images/[name].[ext]'
        }, {
            test: /\.((woff2?|svg)(\?v=[0-9]\.[0-9]\.[0-9]))|(woff2?|svg)$/,
            loader: "url?limit=10000"
        }, {
            test: /\.((ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9]))|(ttf|eot)$/,
            loader: "file"
        }]
    }
}
