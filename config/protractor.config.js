exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    chromeOnly: true,
    directConnect: true,

    capabilities: {
        'browserName': 'chrome'
    },

    specs: ['./../tests/e2e/todo.spec.js'],

    jasmineNodeOpts: {
        showColors: true
    }
};
