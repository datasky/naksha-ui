/* This file contains references to the vendor libraries
 we're using in this project.
 */

/* eslint-disable no-unused-vars */

// css
import './../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './../node_modules/font-awesome/css/font-awesome.min.css';

// js
import 'angular';
import 'angular-ui-router';
import 'angular-ui-bootstrap';
import 'angular-cookies';

// import auth in the end
import './../node_modules/restangular/dist/restangular.min.js';
import './../common/ngmap/build/scripts/ng-map.js';
import './../common/delhivery-auth/dist/delhivery-auth.js';
