function ViewController($scope, NgMap, $DelhiveryLogin, Restangular, MapService) {

    'ngInject';

    $scope.map = MapService.mapInit();

    $scope.version = 0;
    $scope.compareArr = [];
    $scope.locality = {
        polygon:false
    };

    $scope.color = ["#FF0000", "#00FF00", "#0000FF", "#0FFFF0", "#0F0F0F"]
    $scope.fillColor = ['rgba(0, 0, 255, 0.1)', 'rgba(0,255,0, 0.1)', 'rgba( 255,0, 0, 0.1)', 'rgba(225, 0, 255, 0.1)', 'rgba(0, 225, 255, 0.1)']

    Restangular.one('cities').get().then(function(response) {
        $scope.cities = response;
    });

    function goToLocation(focusPoint) {
        var extent = ol.extent.createEmpty();
        ol.extent.extend(extent, focusPoint.getSource().getExtent());
        $scope.map.getView().fit(extent, $scope.map.getSize());
    }

    $scope.getLocalities = function(city) {
        $scope.localities = []
        $scope.clear();
        Restangular.one('localities', city.city_id).get().then(function(response) {
            $scope.localities = response;
        });
    }

    $scope.getLocality = function(locality) {
        $scope.version = 0;
        $scope.compareArr = [];
        Restangular.one('polygon', locality.rec_id).get().then(function(response) {
            if ($scope.locality)
                $scope.map.removeLayer($scope.locality.polygon);

            $scope.locality = response[0]

            $scope.locality.polygon = [MapService.renderPolygon(response[0].polygon, $scope.map, $scope.color[0], $scope.fillColor[0])];
            goToLocation($scope.locality.polygon[0])
        });
    }

    $scope.plotHistory = function(version) {
        $scope.clear();
        $scope.version = version;
        $scope.locality.polygon.push(MapService.renderPolygon($scope.locality.history[version].polygon, $scope.map, $scope.color[version % 5], $scope.fillColor[version % 5]));
        goToLocation($scope.locality.polygon[0]);
    }

    $scope.comparePlot = function() {
        $scope.clear();
        angular.forEach($scope.compareArr, function(version) {
            $scope.locality.polygon.push(MapService.renderPolygon($scope.locality.history[version].polygon, $scope.map, $scope.color[version % 5], $scope.fillColor[version % 5]))
        });
    }

    $scope.select = function(val) {
        if ($scope.compareArr.indexOf(val) > -1) {
            $scope.compareArr.splice($scope.compareArr.indexOf(val), 1);
        } else {
            $scope.compareArr.push(val);
        }
    }

    $scope.clear = function() {
        if (!!$scope.locality.polygon && $scope.locality.polygon.length) {
            console.log($scope.locality.polygon)

            angular.forEach($scope.locality.polygon, function(layer) {
                $scope.map.removeLayer(layer);
            });
        }

        $scope.locality.polygon = [];
    }

};

angular.module(appConfig.name).controller('ViewController', ViewController);
