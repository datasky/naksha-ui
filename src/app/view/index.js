import './view.controller';
import './view.scss';

function routing($stateProvider) {

    'ngInject';

    $stateProvider.
    state('app.view', {
        url: '/view',
        controller: 'ViewController',
        template: require('./view.html'),
        data : { pageTitle: 'View' }
    });
}

angular.module(appConfig.name).config(routing);
