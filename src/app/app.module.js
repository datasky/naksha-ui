import './app.globalConfig.js';

let app = angular.module(appConfig.name, [
    'ui.router',
    'ui.bootstrap',
    'ngCookies',
    'app.environemnt',
    'delhivery.auth',
    // library modules
    'ngMap',
    'restangular'
]);
