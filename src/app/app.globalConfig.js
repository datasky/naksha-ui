let appConfig = {
    name: 'delhivery.starter',
    defaultRoute: 'app.dashboard',
    mobileRoute: 'app.verify'
};

window.appConfig = appConfig;

export default appConfig;
