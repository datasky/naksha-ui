import './shared/directives/title/title.directive.js';
import './shared/directives/sidebar-animation/sidebar-animation.directive.js';
import './shared/directives/dropdown/dropdown.directive.js';
import './shared/services/utils.service.js';
import './shared/services/maps.service.js';


function run($DelhiveryLogin, $rootScope, $state, $cookies) {

    'ngInject';

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {

        if ($DelhiveryLogin.isAuthenticated() && toState.name === 'login') {
            // getUserData();
            if (navigator.userAgent.match(/iPhone | iPad | iPod | Android | mac /i)) {
                $state.transitionTo(appConfig.mobileRoute);
            } else {
                $state.transitionTo(appConfig.defaultRoute);
            }
        }
    });

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {

        if (error === 'user_not_logged_in') {
            event.preventDefault();
            $state.transitionTo("login");
        }
    });

}


angular.module(appConfig.name).run(run);
