function DashboardController($rootScope, $scope, DashboardService, ContentService) {

    'ngInject';

    $scope.stats = DashboardService.stats;
    $scope.facility = DashboardService.facility;
    $scope.facilityNames = ContentService.facilities;
    $scope.selectedFacilityName = ContentService.currentFacility;

    $scope.statusText = {
        'initial': 'Pending to draw',
        'rejected': 'Rejected by FE',
        'pending': 'Redrawn polygons pending for FE verification',
        'skip' : 'Ignored polygons',
        'verified' : 'Verified polygons'
     }

    $scope.$watch(function() { return $rootScope.selectedFacility; }, function(nval) {
        if (nval)
            DashboardService.getDashboardData(nval);
    });

};


function DashboardService($rootScope, Restangular) {

    'ngInject';

    var factObj = {
        stats: []
    }

    factObj.getDashboardData = function(facility_id) {

        if (facility_id) {
            var params = { facilities: [facility_id] }
        } else {
            var params = { facilities: $rootScope.userDetailsObject.user_data.facility_id }
        }

        Restangular.one('dashboard').customPOST(params).then(function(response) {
            angular.copy(response, factObj.stats)
            factObj.facility = facility_id;
        });
    }

    return factObj;

};

angular.module(appConfig.name)
    .controller('DashboardController', DashboardController)
    .service('DashboardService', DashboardService);
