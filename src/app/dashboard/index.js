import './dashboard.controller';
import './dashboard.scss';

function routing($stateProvider) {

    'ngInject';

    $stateProvider.
    state('app.dashboard', {
        url: '/dashboard',
        controller: 'DashboardController',
        template: require('./dashboard.html'),
        data : { pageTitle: 'Dashboard' }
    });
};


angular.module(appConfig.name).config(routing);
