import './verify.controller';
import './verify.scss';

function routing($stateProvider) {

    'ngInject';

    $stateProvider.
    state('app.verify', {
        url: '/verify/:id',
        controller: 'VerifyController',
        template: require('./verify.html'),
        data : { pageTitle: 'Verify' }
    });
};


angular.module(appConfig.name).config(routing);
