function VerifyController($rootScope, $scope, NgMap, Restangular, $state, MapService) {

    'ngInject';

    $scope.map = MapService.mapInit();
    $scope.locality = {};

    var vm = $scope;

    vm.alertClass = '';

    vm.alerts = [];

    function getAlertObject(classToBeApplied, type, msg, timeout = 5000) {
        setTimeout(function() {
            vm.alerts.pop();
            $scope.$apply();
        }, timeout);
        return { class: classToBeApplied, type: type, msg: msg };
    }

    vm.closeAlert = function(index) {
        vm.alerts.splice(index, 1);
    };


    $scope.processAlerts = function(resp) {
        if (!resp.rec_id.length) {
            vm.alerts.push(getAlertObject("info in", "Info!", "There are no more polygons to verify linked to this facility!"));
        } else {
            if ($scope.action === 'vrfd') {
                vm.alerts.push(getAlertObject("success in", "Polygon marked as verified"));
            } else if ($scope.action === 'rjtd') {
                vm.alerts.push(getAlertObject("danger in", "Polygon marked as rejected"));
            }
        }
    }

    $scope.plotPolygon = function(resp) {

        setTimeout(function() {
            if (!!resp && !!resp.rec_id) {
                if($scope.locality.polygon)
                    $scope.map.removeLayer($scope.locality.polygon);

                $scope.locality = resp
                $scope.locality.polygon = MapService.renderPolygon(resp.polygon, $scope.map, '#FF0000', 'rgba(0, 0, 255, 0.1)')
                goToLocation($scope.locality.polygon)
                vm.alerts.pop();
                $scope.$apply();
            }
        }, 2000);

    }

    $scope.getPolygon = function(params) {

        var params = params || {
            facility_id: $rootScope.selectedFacility,
            email: $rootScope.userDetailsObject.email,
            roll: $rootScope.userDetailsObject.user_type,
            name: String($rootScope.userDetailsObject.first_name +" "+ $rootScope.userDetailsObject.last_name),
            action: $scope.action,
            uuid: $rootScope.userDetailsObject.uuid,
            polygon_id: $state.params.id
        }

        $scope.locality = {};

        Restangular.one('verify').customPOST(params).then(function(response) {
            $scope.processAlerts(response);
            $scope.plotPolygon(response);
        });
    }

    function goToLocation(focusPoint) {
        var extent = ol.extent.createEmpty();
        ol.extent.extend(extent, focusPoint.getSource().getExtent());
        $scope.map.getView().fit(extent, $scope.map.getSize());
    }

    $scope.updateStatus = function(action) {

        $scope.action = action;

        var params = {
            facility_id: $rootScope.selectedFacility,
            email: $rootScope.userDetailsObject.email,
            name: String($rootScope.userDetailsObject.first_name +" "+ $rootScope.userDetailsObject.last_name),
            roll: $rootScope.userDetailsObject.user_type,
            uuid: $rootScope.userDetailsObject.uuid,
            action: action,
            polygon_id: $scope.locality.id
        }

        $scope.getPolygon(params);
    }

    $scope.$watch(function() { return $rootScope.selectedFacility; }, function(val) {
        if (val) {
            $scope.getPolygon();
        } else {
            vm.alerts.push(getAlertObject("info in", "Info!", "Please select a facility!"));
        }
    });

};

angular.module(appConfig.name).controller('VerifyController', VerifyController);
