import './env/env.module.js';

function routing($qProvider, $stateProvider, $urlRouterProvider, $locationProvider, RestangularProvider) {

    'ngInject';

    $qProvider.errorOnUnhandledRejections(false);

    // $locationProvider.html5Mode(true).hashPrefix('!');

    $stateProvider.
    state('app', {
            url: '',
            abstract: true,
            views: {
                'header': {
                    template: require('./layout/header/header.html'),
                    controller: 'HeaderController'
                },
                'content': {
                    template: require('./layout/content/content.html'),
                },

                'sidebar@app': {
                    template: require('./layout/sidebar/sidebar.html'),
                    controller: 'SideBarController'
                },
                'footer': {
                    template: require('./layout/footer/footer.html'),
                    controller: 'FooterController'
                }
            },
            data: {
                requireLogin: true
            },
            resolve: {
                load: function($q, $DelhiveryLogin) {

                    var defer = $q.defer();

                    if ($DelhiveryLogin.isAuthenticated()) {
                        defer.resolve('user_logged_in');
                    } else {
                        defer.reject('user_not_logged_in');
                    }
                    return defer.promise;
                }
            }
        })
        .state('login', {
            url: '/login',
            views: {
                'content': {
                    template: require('./login/login.html'),
                }
            },
            data: {
                requireLogin: false
            }
        });

    $urlRouterProvider.otherwise(function($injector) {

        var $state = $injector.get('$state');
        if (navigator.userAgent.match(/iPhone | iPad | iPod | Android /i)) {
            $state.go(appConfig.mobileRoute);
        } else {
            $state.go(appConfig.defaultRoute);
        }

    });


    RestangularProvider.setBaseUrl('https://stg-naksha.delhivery.io/naksha/');
    RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
        if (operation === 'put') {
            elem._id = undefined;
        }
        return elem;
    });

    RestangularProvider.setRequestSuffix('/');

};

angular.module(appConfig.name).factory('httpRequestInterceptor', function() {
    return {
        request: function(config) {

            // use this to destroying other existing headers
            // config.headers = {Authorization:'Token 5e210b0911786115a7a95af3e575bc2cebfebf10'}
            
            // use this to prevent destroying other existing headers
            if (!config.header) {
                config.headers['Authorization'] = 'Token 5a0e05d01b138649dfcf5a2e016df4f49cbf8ed8';
            }

            return config;
        }
    };
});

angular.module(appConfig.name).config(function($httpProvider) {
    $httpProvider.interceptors.push('httpRequestInterceptor');
});

function setLoginConfiguration($DelhiveryLoginProvider, environmentVariables) {

    'ngInject';

    $DelhiveryLoginProvider.init({
        environment: 'prod',
        needGoogleLogin: true
    });
};


angular.module(appConfig.name).config(routing).config(setLoginConfiguration);
