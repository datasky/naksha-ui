function LoginController($scope, $state, $rootScope) {

    'ngInject';

    $scope.login = function() {
        $rootScope.loggedIn = true;

        // if (navigator.userAgent.match(/iPhone | iPad | iPod | Android /i)) {
        //     $state.go('app.verify');
        // } else {
        //     $state.go('app.dashboard');
        // }
    }

    $scope.logout = function() {

        $rootScope.loggedIn = false;
        $state.go('login');
    };
};

angular.module(appConfig.name).controller('LoginController', LoginController);
