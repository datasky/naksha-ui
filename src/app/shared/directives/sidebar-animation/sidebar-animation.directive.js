// code for Material animation
angular.module(appConfig.name).directive('leftMenuToggle', [function() {
    return {
        link: function(scope, element, attrs) {
            element.on('click', function(event) {
                angular.element(document.querySelector("#sidebar-wrapper")).toggleClass("collapsed");
            });
        }
    };
}]);