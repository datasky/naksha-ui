import './dropdown.scss';

angular.module(appConfig.name).directive('searchBar', ($window) => {
    return {
        replace: true,
    	restrict: 'E',
        scope: {
            list: '=',
            showField: '@',
            placeholder: '@',
            selectFxn: '&',
            focusFxn: '&',
            ngModel: '=',
            dropLimit: '@',
            disable: '@'
        },
        link: (scope, el, attrs) => {
            scope.elemFocus = false
            scope.seeOptions = false
            scope.dropLimit = Number(scope.dropLimit) || 5

            el.unbind().bind('keyup', (event) => {
                scope.seeOptions = true
                // console.log($('.option-wrapper'))
                // console.log(el)
                var allOptions = angular.element(angular.element(el.children('.option-wrapper')[1]).children('.search-options')).children('.option')
                if (allOptions.length) {
                    scope.elemFocus = scope.elemFocus || 0
                    allOptions.removeClass('active')
                    if (event.which === 40) {
                        if (scope.elemFocus != allOptions.length - 1) {
                            scope.elemFocus += 1
                        }
                    } else if (event.which === 38) {
                        if (scope.elemFocus != 0) {
                            scope.elemFocus -= 1
                        }
                    } else if (event.which === 13) {
                        allOptions[scope.elemFocus].click()
                    }

                    if (scope.seeOptions) {
                        allOptions[scope.elemFocus].scrollIntoView({ block: 'start', behavior: 'smooth' })
                        angular.element(allOptions[scope.elemFocus]).addClass('active')
                    }

                    event.stopPropagation()
                    event.preventDefault()
                } else {
                    scope.elemFocus = false
                }
            })

            scope.fxn = (item, event) => {
                scope.ngModel = item
                scope.seeOptions = false
                scope.elemFocus = false
                scope.searchString = scope.mapItem(item)
                event.stopPropagation()
                event.preventDefault()

                if (scope.selectFxn()) { scope.selectFxn()[0](item, true) }
            }

            scope.mapItem = (item) => {
                if (scope.showField) {
                    var displayLabel = ''

                    angular.forEach(scope.showField.split(','), (value) => {
                        displayLabel += item[value] + ' : '
                    })

                    return displayLabel.substr(0, displayLabel.length - 3)
                } else {
                    return item
                }
            }

            scope.setFocus = () => {
                el.children('input')[0].select()
                angular.element(document.querySelector('.search-options')).css({ 'display': 'none' })
                scope.seeOptions = true
                el.children('.option-wrapper').children('.search-options').css({ 'display': 'block' })
                if (scope.focusFxn()) { scope.focusFxn()[0]() }
            }

            scope.focusLost = (item) => {
                scope.seeOptions = false
            }

            scope.$watch(() => {
                return scope.ngModel
            }, (val) => {
                if (val) {
                    scope.searchString = scope.mapItem(scope.ngModel)
                } else {
                    scope.searchString = ''
                }
            })
        },
        template: ['<div class="search-bar"  >',
            '<input class="" type="" ng-disabled="{{ disable }}" placeholder="{{placeholder}}" ng-blur="focusLost($event);" ng-focus="setFocus()" ng-model="searchString" ng-change="elemFocus=false;">',
            '<div class="option-wrapper"><div class="search-options" ng-show="seeOptions">',
            '<p class="option" ng-repeat="item in list | filter : searchString | limitTo: dropLimit | orderBy " ng-bind="mapItem(item);" ng-mousedown="fxn(item,$event);" ng-click="fxn(item,$event);"></p>',
            '</div></div>',
            '</div>'
        ].join('')
    }
})
