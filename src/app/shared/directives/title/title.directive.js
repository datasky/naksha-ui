function title($rootScope, $timeout, $state) {

    'ngInject';

    return {
        restrict: 'A',
        link: function() {

            $rootScope.title = $state.current.data.pageTitle;

            var listener = function(event, toState) {

                $timeout(function() {
                    $rootScope.title = (toState.data && toState.data.pageTitle) ? toState.data.pageTitle : 'Default title';
                });
            };

            $rootScope.$on('$stateChangeSuccess', listener);

        }
    };
};


angular.module(appConfig.name).directive('title', title);
