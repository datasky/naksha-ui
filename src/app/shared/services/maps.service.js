function MapService() {
    var _utils = this,
        _map = false;

    _utils.markers = {
        // pins
        "pin-red": 'https://maps.google.com/mapfiles/ms/icons/red-dot.png',
        "pin-yellow": 'https://maps.google.com/mapfiles/ms/icons/yellow-dot.png',
        "pin-blue": 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
        "pin-green": 'https://maps.google.com/mapfiles/ms/icons/green-dot.png',

        //dots

        "dot-red": '/mapper/static/img/marker_red_small.png',
        "dot-green": '/mapper/static/img/marker_green_small.png',
        "dot-blue": '/mapper/static/img/marker_blue_small.png',
        "dot-grey": '/mapper/static/img/marker_grey_small.png',
        "dot-purple": '/mapper/static/img/marker_purple_small.png',
        "dot-yellow": '/mapper/static/img/marker_yellow_small.png',
        "dot-black": '/mapper/static/img/marker_black_small.png',
        "dot-aqua": '/mapper/static/img/marker_aqua_small.png',
        "dot-white": '/mapper/static/img/marker_white_small.png',
    }

    _utils.renderedMarkers = [];

    _utils.mapInit = function() {
        _utils.raster = new ol.layer.Tile({
            source: new ol.source.OSM({
                url: 'https://mt0.mapmyindia.com/advancedmaps/v1/1ghv58m8rzbuv3rkmz1rdicev5awoafo/still_map/{z}/{x}/{y}.png'
            })
        });

        _utils._source = new ol.source.Vector({ wrapX: false });

        _utils.vector = new ol.layer.Vector({
            source: _utils._source
        });

        _utils._map = new ol.Map({
            layers: [_utils.raster, _utils.vector],
            target: 'map',
            view: new ol.View({
                center: ol.proj.transform([80, 22], 'EPSG:4326', 'EPSG:3857'),
                // center: [80,20],
                zoom: 5
            })
        });

        return _utils._map;
    }

    _utils.renderPolygon = function(coordArray, map, color, fillColor) {

        // console.log(coordArray[0].map((a)=> { return [a.lng,a.lat]; }))
        var polygon = false;

        if(coordArray[0][0] instanceof Array){
            polygon = new ol.geom.Polygon(coordArray);
        }else{
            polygon = new ol.geom.Polygon([coordArray[0].map((a) => { return [a.lng, a.lat]; })]);
        }

        polygon.transform('EPSG:4326', 'EPSG:3857');
        // Create feature with polygon.
        var feature = new ol.Feature(polygon);

        // Create vector source and the feature to it.
        var vectorSource = new ol.source.Vector();
        vectorSource.addFeature(feature);

        var style = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: color,
                width: 5
            }),
            fill: new ol.style.Fill({
                color: fillColor
            })
        })

        // Create vector layer attached to the vector source.
        if(!_utils._vectorLayer){
            _utils._vectorLayer = new ol.layer.Vector({
                source: vectorSource,
                style: style
            });
        } else {
            _utils._map.removeLayer(_utils._vectorLayer);

            _utils._vectorLayer.setSource(vectorSource);
            _utils._vectorLayer.setStyle(style);
        }

        // Add the vector layer to the map.
        _utils._map.addLayer(_utils._vectorLayer);

        return _utils._vectorLayer;
    }

    _utils.draw = function(type) {
        // var source = new ol.source.Vector({wrapX: false});

        if (type !== 'None') {
            var draw = new ol.interaction.Draw({
                source: _utils._source,
                type: /** @type {ol.geom.GeometryType} */ (type)
            });
            
            _utils._map.addInteraction(draw);

            return draw;
        }
    }

    _utils.renderMarker = function(item, markerSet, map) {
        var marker = new google.maps.Marker({
            position: { lat: item.lat + (Math.random() * 0.0005), lng: item.lng + (Math.random() * 0.0005) },
            map: map,
            attrs: item,
            markerSet: markerSet,
            icon: _utils.markers[markerSet[0]]
        });
        _utils.renderedMarkers.push(marker);
        return marker;
    }

    _utils.focusMap = function(arr, map, zoom) {
        var bounds = new google.maps.LatLngBounds();
        arr.map((pts) => { bounds.extend(new google.maps.LatLng(Number(pts.lat), Number(pts.lng))) });
        map.fitBounds(bounds);
        if (zoom) {
            map.setZoom(zoom);
        } else if (arr.length === 1) {
            map.setZoom(12);
        }
    }

    _utils.setInfoWindow = function(marker, map, content) {

        var infowindow = new google.maps.InfoWindow({
            content: content || 'No info available'
        })

        google.maps.event.addListener(marker, 'mouseover', (e) => {
            infowindow.open(map, marker)
        })

        google.maps.event.addListener(marker, 'mouseout', (e) => {
            infowindow.close()
        })
    }

    _utils.resetMap = function() {
        _utils.renderedMarkers.map((a) => { a.setMap(null); });
    }

};

angular.module(appConfig.name).service('MapService', MapService);
