function utils() {
    var _utils = this;
    
    _utils.mvcToArray = function(MVCArray) {
        return MVCArray.map(function(a) {
                return [a.lng(), a.lat()];
            });
        };

    _utils.mvcToLatLng = function(MVCArray) {
    	return MVCArray.map(function(a) {
                return {lng:a.lng(), lat:a.lat()};
            });
        };

    _utils.latLngToArray = function(MVCArray) {
        return MVCArray.map(function(a) {
                return [a.lng, a.lat];
            });
        };

    _utils.arraytoLngLat = function(arr) {
        return arr.map(function(a) {
                return [a[1], a[0]];
            });
        };
    };

    angular.module(appConfig.name).service('utils', utils);
