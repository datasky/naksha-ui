class StringService {
    constructor() {

        //public functions.
        this.numberFormat = function(string) {

            try {
                string = string.toString();
                var lastThree = string.substring(string.length - 3);
                var otherNumbers = string.substring(0, string.length - 3);
                if (otherNumbers !== '')
                    lastThree = ',' + lastThree;
                var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
                return res;

            } catch (err) {
                console.log("Invalid input " + err);
                return input;
            }
        }

        this.slugToLabel = function(string) {

            if (typeof string === 'number') {
                return string;
            }

            if (string) {
                return string.replace(/-/g, ' ').replace(/\w\S*/g, function(text) {
                    return text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
                });
            }
        }

        this.labelToSlug = function(string) {

            try {
                var data = string.toLowerCase();
                return data.split(' ').join('-');
            } catch (error) {
                console.log("Invalid input " + error);
            }
        }

        this.numberSuffix = function(string) {
            try {
                var s = ["th", "st", "nd", "rd"],
                    v = string % 100;
                return string + (s[(v - 20) % 10] || s[v] || s[0]);
            } catch (error) {
                console.log("Invalid input " + error);
            }
        }

        this.substring = function(str, start, end) {
            return str.substring(start, end);
        }
    }
}

angular.module(appConfig.name).service('StringService', StringService);
