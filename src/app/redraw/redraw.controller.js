function RedrawController($rootScope, $scope, NgMap, Restangular, $http, utils, RedrawService, MapService) {

    'ngInject';

    $scope.overlay = false;
    $scope.basePolygon = [];

    $scope.statusMsg = {
        'default': "Polygon not available",
        'initial': "Polygon to be verified",
        'rejected': "Polygon rejected by FE"
    }

    $scope.facs = RedrawService.polygonList;
    $scope.map = MapService.mapInit();
    $scope.drawingManager = false;

    var vm = $scope;

    vm.alertClass = '';

    vm.alerts = [];

    function getAlertObject(classToBeApplied, type, msg, timeout = 10000) {
        setTimeout(function() {
            vm.alerts.pop();
            $scope.$apply();
        }, timeout);
        return { class: classToBeApplied, type: type, msg: msg };
    }

    vm.closeAlert = function(index) {
        vm.alerts.splice(index, 1);
    };


    $scope.processAlerts = function(resp) {
        if (!resp.length && !resp.rec_id.length) {
            vm.alerts.push(getAlertObject("info in", "Info!", "There are no more polygons linked to this facility!"));
        } else {
            if ($scope.action) {
                vm.alerts.push(getAlertObject("success in", "Success!", "Polygon saved!"));
            }
        }
    }

    $scope.plotPolygon = function(resp) {
        setTimeout(function() {
            if (!!resp && !!resp.rec_id) {
                if ($scope.locality.polygon){
                    $scope.map.removeLayer($scope.locality.polygon);
                }

                $scope.locality = resp;
                angular.copy(resp.polygon, $scope.basePolygon);
                $scope.locality.polygon = MapService.renderPolygon(resp.polygon, $scope.map, '#FF0000', 'rgba(0, 0, 255, 0.1)')
                goToLocation($scope.locality.polygon)
                vm.alerts.pop();
                $scope.$apply();
            }
        }, 2000);
    }

    $scope.getPolygon = function(params) {
        $scope.locality = {};
        Restangular.one('polygon', params.polygon_id).get().then(function(response) {
            if (response.length) {
                $scope.plotPolygon(response[0]);
                $scope.action = 'default';
            } else {
                $scope.locality = response;
                $scope.locality.polygon = $scope.basePolygon = [Number(response.lng) , Number(response.lat)]
                goToLocation($scope.basePolygon,5)
            }
        });
    }


    function goToLocation(focusPoint, zoom) {
        if (focusPoint instanceof Array) {
            var point = new ol.geom.Point(focusPoint);
            point.transform('EPSG:4326', 'EPSG:3857');
            $scope.map.getView().fit(point, {minResolution: zoom});
        } else {
            var extent = ol.extent.createEmpty();
            ol.extent.extend(extent, focusPoint.getSource().getExtent());
            $scope.map.getView().fit(extent, $scope.map.getSize());
        }
    }

    $scope.updateStatus = function(action) {

        $scope.map.removeInteraction($scope.drawingManager)
        switch (action) {
            case 'save':


                var tempPoly = $scope.locality.polygon.getSource().getFeatures()[0].getGeometry().transform('EPSG:3857','EPSG:4326').getCoordinates();

                var params = {
                    polygon: utils.latLngToArray(tempPoly),
                    polygon_id: $scope.locality.id,
                    rec_id: $scope.locality.rec_id,
                    roll: $rootScope.userDetailsObject.user_type,
                    facility_id: $rootScope.selectedFacility,
                    email: $rootScope.userDetailsObject.email,
                    name: String($rootScope.userDetailsObject.first_name + " " + $rootScope.userDetailsObject.last_name),
                    uuid: $rootScope.userDetailsObject.uuid,
                    action: $scope.action
                };

                Restangular.one('draw').customPOST(params).then(function(response) {
                    // $scope.processAlerts(response);
                    vm.alerts.push(getAlertObject("success in", "Success!", "Polygon saved!"));
                    $scope.locality = {};
                    RedrawService.getPolygonListing();
                    $scope.action = 'default';
                    goToLocation([{ lat: 22, lng: 82 }], 5);
                }).catch(function(err) {
                    vm.alerts.push(getAlertObject("danger in", "", "Invalid polygon submitted!"));
                });
                break;
            case 'reset':
                $scope.locality.polygon = false;
                $scope.polyArray = [];
                $scope.action = 'default';
                setTimeout(function() {
                    $scope.map.removeLayer($scope.locality.polygon)
                    $scope.locality.polygon = MapService.renderPolygon($scope.basePolygon, $scope.map, '#FF0000', 'rgba(0, 0, 255, 0.1)')
                    $scope.$apply();
                }, 1000);

                break;
            case 'redraw':
                $scope.polyArray = [];
                $scope.action = 'redrawn';
                $scope.map.removeLayer($scope.locality.polygon)
                $scope.locality.polygon = false;

                $scope.drawingManager = MapService.draw('Polygon');

                $scope.drawingManager.on('drawend',function(e){
                    vm.alerts.push(getAlertObject("success in", "Success!", "Polygon completed!"));
                    $scope.locality.polygon = MapService.renderPolygon(e.feature.getGeometry().transform('EPSG:3857','EPSG:4326').getCoordinates(), $scope.map, '#FF0000', 'rgba(0, 0, 255, 0.1)')
                    $scope.map.removeInteraction($scope.drawingManager);     
                    $scope.$apply();
                });

                break;
        }
    }

    $scope.$watch(function() { return $rootScope.selectedFacility; }, function(val) {
        if (val) {
            RedrawService.getPolygonListing();
        } else {
            vm.alerts.push(getAlertObject("info in", "Info!", "Please select a facility!"));
        }
    });

    $scope.$watch(function() { return RedrawService.polygonList; }, function(a, o) {
        setTimeout(function() {
            if (!a.length && $rootScope.selectedFacility) {
                vm.alerts.push(getAlertObject("info in", "Info!", "There are no more polygons linked to this facility!"));
            }
            $scope.$apply();
        }, 3000);

    });
};

function RedrawService(Restangular, $rootScope) {

    'ngInject';

    var factObj = {
        polygonList: []
    }

    factObj.getPolygonListing = function() {
        if ($rootScope.selectedFacility) {
            factObj.polygonList.length = 0;
            Restangular.one('facility_localities/' + $rootScope.selectedFacility).get().then(function(response) {
                angular.copy(response.data, factObj.polygonList)
                factObj.facility = facility_id;
            });
        }
    }

    return factObj;

};

angular.module(appConfig.name)
    .controller('RedrawController', RedrawController)
    .service('RedrawService', RedrawService);
