import './redraw.controller';
import './redraw.scss';

function routing($stateProvider) {

    'ngInject';

    $stateProvider.
    state('app.redraw', {
        url: '/redraw/:id',
        controller: 'RedrawController',
        template: require('./redraw.html'),
        data: { pageTitle: 'Redraw' }
    });
};


angular.module(appConfig.name).config(routing);
