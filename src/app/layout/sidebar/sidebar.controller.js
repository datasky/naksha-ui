import './sidebar.scss';

function SideBarController($scope, $state,$rootScope) {

    'ngInject';

    $scope.isActive = function(stateName) {
        return $state.current.name === stateName;
    };

    $scope.$watch(function() { return $rootScope.userDetailsObject;},function(val){
        if(val){
            if(val.user_type === "OFR"){
                $scope.menuLinks = [{
                    name: 'Verify',
                    link: 'app.verify',
                    class: 'fa fa-circle'
                }]
            } else {
                $scope.menuLinks = [{
                    name: 'Dashboard',
                    link: 'app.dashboard',
                    class: 'fa fa-circle'
                },{
                    name: 'Verify',
                    link: 'app.verify',
                    class: 'fa fa-circle'
                },{
                    name: 'View',
                    link: 'app.view',
                    class: 'fa fa-circle'
                },{
                    name: 'Redraw',
                    link: 'app.redraw',
                    class: 'fa fa-circle'
                }]
            }
        }
    })

};

angular.module(appConfig.name).controller('SideBarController', SideBarController);
