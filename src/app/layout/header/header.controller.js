import './header.scss';

function HeaderController($rootScope, $http, $scope, $window, $DelhiveryLogin, $state, DashboardService, RedrawService, ContentService) {

    'ngInject';

    $scope.profile = {
        clientName: 'User'
    };

    $scope.appName = 'Naksha';

    ContentService.getAllFacilities();
    $scope.userdata = {
        facility_names: ContentService.facility_names
    }

    $scope.logout = function() {

        delete $rootScope.userdata;

        $DelhiveryLogin.logout();
        $rootScope.loggedIn = false;

        $state.go('login');
    };


    $scope.getDashStats = function(item) {
        $rootScope.selectedFacility = item.facility_id;
        DashboardService.getDashboardData(item.facility_id);
        // RedrawService.getPolygonListing();
    }

};


angular.module(appConfig.name).controller('HeaderController', HeaderController);
