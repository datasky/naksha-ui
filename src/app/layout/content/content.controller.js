function ContentController($scope) {

    'ngInject';

    // ContentService.getAllFacilities();

};

function ContentService($rootScope, Restangular, $window, $DelhiveryLogin, $http, $state, DashboardService) {

    'ngInject';

    var factObj = {
        facilities: {},
        currentFacility: {},
        facility_names: []
    }

    factObj.getUserData = function() {

        var parseJwt = function(token) {
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace('-', '+').replace('_', '/');
            return JSON.parse($window.atob(base64));
        }

        var data = parseJwt($DelhiveryLogin.getUserJWT());

        var params = {
            custom: true,
            token: 'Bearer ' + $DelhiveryLogin.getUserJWT()
        }

        $http({
            method: 'GET',
            url: 'https://api-ums.delhivery.com/v2/users/' + data.sub + '/',
            header: params
        }).then(function successCallback(response) {
            $rootScope.userDetailsObject = response.data;

            if (response.data.user_data.facility_id.length < 3) {
                $rootScope.selectedFacility = response.data.user_data.facility_id[0];
                angular.copy({ facility_id: $rootScope.selectedFacility, facility_name: factObj.facilities[$rootScope.selectedFacility] }, factObj.currentFacility);
                angular.copy(response.data.user_data.facility_id.map(function(a) {
                    return { facility_id: a, facility_name: factObj.facilities[a] };
                }), factObj.facility_names);
            } else {

                angular.copy(response.data.user_data.facility_id.map(function(a) {
                    return { facility_id: a, facility_name: factObj.facilities[a] };
                }), factObj.facility_names);

                DashboardService.getDashboardData();
            }

            if (response.data.user_type === "OFR") {
                $state.go('app.verify');
            // } else {
            //     $state.go('app.dashboard');
            }

        }, function errorCallback(response) {
            $state.go('login');
        });

    }

    factObj.getAllFacilities = function() {
        Restangular.one('all_dc').get().then(function(response) {
            angular.copy(response, factObj.facilities);
            factObj.getUserData();
        });
    };

    return factObj;

};


angular.module(appConfig.name)
    .controller('ContentController', ContentController)
    .service('ContentService', ContentService);
