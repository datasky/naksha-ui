if (module.hot) {
    module.hot.accept();
}

/* eslint-disable no-unused-vars */

// app js
import './app/app.module.js';
import './app/app.config.js';
import './app/app.run.js';

// design framework styles
import './styles/app.scss';

// app pages / modules
import './app/layout';
import './app/login';
import './app/dashboard';
import './app/verify';
import './app/view';
import './app/redraw';
